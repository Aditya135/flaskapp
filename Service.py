import pandas as pd
from pandas import json_normalize
import requests
import xlwt

def createExcelSheet(df):
    book = xlwt.Workbook()
    style = xlwt.XFStyle()
    font = xlwt.Font()
    font.bold = True
    style.font = font
    grouped = df.groupby("Date")
    for name, group in grouped:
        try:
            sheet = book.add_sheet(name)
        except:
            print(name)
            continue
        row = sheet.row(0)
        row.write(0,"DateTime",style=style)
        row.write(1,"Length",style=style)
        row.write(2,"Weight",style=style)
        row.write(3,"Quantity",style=style)

        for idx, value in enumerate(group.iterrows()):
            row = sheet.row(idx+1)
            row.write(0,value[1]["DateTime"])
            row.write(1,value[1]["Length"])
            row.write(2,value[1]["Weight"])
            row.write(3,value[1]["Quantity"])
    book.save("outputs/test.xls")
    return True

if __name__ == "__main__":
    URL = "https://assignment-machstatz.herokuapp.com/excel"
    r = requests.get(URL)
    data = r.json()
    data = json_normalize(data)
    data["DateTime"] = data["DateTime"].apply(lambda x: x[:10])
    createExcelSheet(data)