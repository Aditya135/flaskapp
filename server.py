from flask import Flask, request, jsonify, send_from_directory, abort
import requests, pandas
from pandas import json_normalize
from Service import createExcelSheet
import os
URL = "https://assignment-machstatz.herokuapp.com/excel"

app = Flask(__name__)
app.config["DEBUG"] = True
def reformat_datestring(date):
    return date[6:]+"-"+date[3:5]+"-"+date[:2]

@app.route("/total",methods=["GET"])
def home():
    r = requests.get(URL)
    data = r.json()
    data = json_normalize(data)
    data["DateTime"] = data["DateTime"].apply(lambda x: x[:10])
    qry_date = reformat_datestring(request.args['day'])
    filtered_data = data[data["DateTime"]==qry_date]
    print(filtered_data)

    return jsonify({"totalWeight" : str(filtered_data.Weight.sum()),"totalLength" : str(filtered_data.Length.sum()), "totalQuantity" : str(filtered_data.Quantity.sum())})

@app.route("/excelreport",methods=["GET"])
def getExcel():
    r = requests.get(URL)
    data = r.json()
    data = json_normalize(data)
    data["Date"] = data["DateTime"].apply(lambda x: x[:10])
    try:
        os.remove("outputs/test.xls")
    except:
        pass
    res = createExcelSheet(data)
    if(res):
        return send_from_directory("outputs",filename="test.xls",as_attachment=True)

@app.route("/test",methods=["GET"])
def test():
    return "HELLO WORLD"
    
if __name__ == "__main__":
    app.run()